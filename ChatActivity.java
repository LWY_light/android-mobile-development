package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ChatActivity extends AppCompatActivity {

        TextView textView;
        Button button;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Log.d("lwy","chat:onCreate...");//创建日志
        textView=findViewById(R.id.textView_Chat);
        button=findViewById(R.id.button_return);

        //2怎么收消息
        Intent intent=getIntent();//不能在new否则是新的

        String str2="我和"+intent.getExtras().getString("name")+"的聊天界面";

        textView.setText(str2);//把消息塞进textview

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("lwy","chat:onStart...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("lwy","chat:onStop...");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("lwy","chat:onPostResume...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("lwy","chat:onDestroy...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("lwy","chat:onResume...");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("lwy","chat:onRestart...");
    }
}