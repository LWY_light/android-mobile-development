package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class Fragment2 extends Fragment {
    RecyclerView recyclerview;
    List<String> list;
    Myadapter adapter;
    Context context;

    @SuppressLint("MissingInflatedId")

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab2, container, false);
        recyclerview= view.findViewById(R.id.recyclerview);

        list=new ArrayList<String>();//是一个数组，但是链表存储

        context=getContext();

        list.add("母后大人");
        list.add("爹");
        for (int i=1;i<=6;i++){
            list.add("宝贝"+i+"号");
        }

        adapter= new Myadapter(context,list);//list给adpter

        recyclerview.setAdapter(adapter);

        LinearLayoutManager manager=new LinearLayoutManager(context);
        manager.setOrientation(RecyclerView.VERTICAL);
        recyclerview.setLayoutManager(manager);//控制列表上下左右
        // Inflate the layout for this fragment
        return view;//压缩，整个屏幕的xml压缩到局部
    }
}