package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Fragment fragment1,fragment2,fragment3,fragment4;

    FragmentManager fm;//fragment管理器
    //FragmentTransaction ft;//fragment交互（通信一对一，交互是批量动作，带commit），放外面不利于事务独立性，函数都可以用

    LinearLayout linearLayout1,linearLayout2,linearLayout3,linearLayout4;

    @SuppressLint("MissingInflatedId")

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        linearLayout1=findViewById(R.id.LinearLayout1);
        linearLayout2=findViewById(R.id.LinearLayout2);
        linearLayout3=findViewById(R.id.LinearLayout3);
        linearLayout4=findViewById(R.id.LinearLayout4);

        fragment1=new Fragment1();
        fragment2=new Fragment2();
        fragment3=new Fragment3();
        fragment4=new Fragment4();
        fm=getSupportFragmentManager();//new用不了用get,new要重写构造函数

        initial();//初始化

        fragmenthide();//隐藏字幕
        fragmentshow(fragment1);//默认聊天界面

        //屏幕click监听,全局监听以免代码冗余
        linearLayout1.setOnClickListener(this);
        linearLayout2.setOnClickListener(this);
        linearLayout3.setOnClickListener(this);
        linearLayout4.setOnClickListener(this);

    }

    private void fragmenthide() {
        FragmentTransaction ft=fm.beginTransaction()
                .hide(fragment1)
                .hide(fragment2)
                .hide(fragment3)
                .hide(fragment4);//隐藏之后涉及点击功能
        ft.commit();
    }//初始隐藏字幕

    private void initial() {

        FragmentTransaction ft=fm.beginTransaction()
                .add(R.id.content1,fragment1)
                .add(R.id.content1,fragment2)
                .add(R.id.content1,fragment3)
                .add(R.id.content1,fragment4);
        ft.commit();//get也没有，交互实例基于管理器,以commit形式提交，ft是事务执行对象，commit前面的都是事务,commit和执行要分开进行
    }//初始化

    @Override
    public void onClick(View view) {

        //点一个再点一个会叠加？

        fragmenthide();

        int id=view.getId();
        if(id==R.id.LinearLayout1) {
            fragmentshow(fragment1);
        }
        if(id==R.id.LinearLayout2) {
            fragmentshow(fragment2);
        }
        if(id==R.id.LinearLayout3){
            fragmentshow(fragment3);
        }
        if(id==R.id.LinearLayout4) {
            fragmentshow(fragment4);
        }


    }//点击

    private void fragmentshow(Fragment fragment) {
        FragmentTransaction ft=fm.beginTransaction()
                .show(fragment);
        ft.commit();
    }//显示
}