package com.example.mylovechat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ChatActivity extends AppCompatActivity {

    TextView textView;
    ImageButton imageButtonReturn;
    @SuppressLint({"MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Log.d("lwy","chat:onCreate...");//创建日志
        textView=findViewById(R.id.textView_Chat);
        imageButtonReturn=findViewById(R.id.imageButton_return);

        //2怎么收消息
        Intent intent=getIntent();
        String str2=intent.getExtras().getString("name");
        textView.setText(str2);
        imageButtonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("lwy","chat:onStart...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("lwy","chat:onStop...");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("lwy","chat:onPostResume...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("lwy","chat:onDestroy...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("lwy","chat:onResume...");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("lwy","chat:onRestart...");
    }
}