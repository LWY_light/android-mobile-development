package com.example.mylovechat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Fragment1 extends Fragment {

    ImageView imageView;
    TextView textView1,textView2,textView3;
    Button button_pre,button_next,button_love;
    int i=-1;
    MyThread thread;
    //图片数据
    int[] img={R.drawable.img,R.drawable.img_6,R.drawable.img_2
            ,R.drawable.img_3,R.drawable.img_4,R.drawable.img_5};

    ArrayList<HashMap<String,Object>> list;//value不写死，写object
    Context context;
    private ContentResolver resolver;
    private static final String[] PROJECTION = new String[]{"id", "name", "age"};
    private static final String AUTHORITY ="lwy.light.provider";
    private static final Uri NOTIFY_URI=Uri.parse("content://"+AUTHORITY+"/person");
    Uri uri=Uri.parse("content://lwy.light.provider/person");

    public class MyThread extends Thread{
        Handler handler;
        @Override
        public void run() {
            Looper.prepare();//收货地址
            handler=new Handler(Looper.myLooper()){  //handler和looper连接起来
                @Override
                public void handleMessage(@NonNull Message msg) {
                    list=MyqueryAll(resolver.query(uri, context.databaseList(),
                            "select * from person",new String[]{},null));
                    if (msg.what==999){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                i++;
                                if (i >= list.size()) i = 0;
                                textView1.setText(list.get(i).get("name").toString());
                                String str=list.get(i).get("name").toString();
                                Log.d("lwy","name"+str+"-"+i);
                                textView2.setText(" 编号:"+list.get(i).get("id").toString());
                                textView3.setText(" 年龄:"+list.get(i).get("age").toString());
                                if(i<img.length) imageView.setImageResource(img[i]);
                                else imageView.setImageResource(R.drawable.img_7);
                            }
                        });
                    }
                    if (msg.what==666){
                        String str=msg.getData().getString("name");
                        Log.d("lwy","name"+str);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                i--;
                                if (i < 0) i = list.size() - 1;
                                textView1.setText(list.get(i).get("name").toString());
                                textView2.setText(" 编号:"+list.get(i).get("id").toString());
                                textView3.setText(" 年龄 :"+list.get(i).get("age").toString());
                                if(i<img.length) imageView.setImageResource(img[i]);
                                else imageView.setImageResource(R.drawable.img_7);
                            }
                        });
                    }

                    button_love.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(context,ChatActivity.class);
                            intent.putExtra("name",list.get(i).get("name").toString());
                            context.startActivity(intent);
                        }
                    });
                }
            };
            Looper.loop();  //对queue中消息循环取
        }
    }

    private void updateList() {
        list = MyqueryAll(resolver.query(uri, context.databaseList(),
                "select * from person", new String[]{}, null));
    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.tab1, container, false);
        imageView=view.findViewById(R.id.imageView_love);
        button_pre=view.findViewById(R.id.button_pre);
        button_next=view.findViewById(R.id.button_next);
        button_love=view.findViewById(R.id.button_love);
        textView1=view.findViewById(R.id.textView_love);
        textView2=view.findViewById(R.id.textView_love_id);
        textView3=view.findViewById(R.id.textView_love_age);

        context=getContext();
        resolver=context.getContentResolver();

        thread=new MyThread();
        thread.start();
        button_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message2=new Message();
                message2.what=666;
                Bundle bundle=new Bundle();
                bundle.putString("name","lwy");
                message2.setData(bundle);
                thread.handler.sendMessage(message2);
            }
        });
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Message message2=new Message();
                message2.what=999;
                Bundle bundle=new Bundle();
                bundle.putString("name","lwy");
                message2.setData(bundle);
                thread.handler.sendMessage(message2);
            }
        });

        return view;//压缩，整个屏幕的xml压缩到局部
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("lwy","onResume...");
        updateList(); // 在Fragment恢复之后更新list
    }

    public ArrayList<HashMap<String,Object>> MyqueryAll(Cursor cursor1) {
        Log.d("lwy","fra1-"+cursor1.getCount());
        Cursor cursor=cursor1;
        ArrayList<HashMap<String,Object>> list = new ArrayList<>();;//value不写死，写object
        cursor.moveToFirst();//cursor每次走到头遍历
        while(!cursor.isAfterLast()){
            @SuppressLint("Range") int id=cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String name=cursor.getString(cursor.getColumnIndex("name"));
            @SuppressLint("Range") int age=cursor.getInt(cursor.getColumnIndex("age"));
            //stringBuilder.append(id+name+age);
            HashMap<String,Object> map=new HashMap<>();
            map.put("id",id);
            map.put("name",name);
            map.put("age",age);
            list.add(map);
            cursor.moveToNext();
        }

        cursor.close();
        return list;
    }
}