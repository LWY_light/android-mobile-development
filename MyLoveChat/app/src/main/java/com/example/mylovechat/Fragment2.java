package com.example.mylovechat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Fragment2 extends Fragment {

    ImageButton addButton;
    RecyclerView recyclerview;
    ArrayList<HashMap<String,Object>> list;//value不写死，写object
    ArrayList<HashMap<String,Object>> imagedata; //图片数据
    Myadapter adapter;
    Context context;
    private ContentResolver resolver;
    private static final String[] PROJECTION = new String[]{"id", "name", "age"};
    private static final String AUTHORITY ="lwy.light.provider";
    private static final Uri NOTIFY_URI=Uri.parse("content://"+AUTHORITY+"/person");
    Uri uri=Uri.parse("content://lwy.light.provider/person");

    @SuppressLint("MissingInflatedId")

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab2, container, false);
        recyclerview= view.findViewById(R.id.recyclerview);
        addButton=view.findViewById(R.id.imageButton_add2);

        context=getContext();
        resolver=context.getContentResolver();

        Uri uri1=NOTIFY_URI;
        ContentValues values1=new ContentValues();
        values1.put("name","王也");
        values1.put("age",26);
        resolver.insert(uri1,values1);
        ContentValues values2=new ContentValues();
        values2.put("name","诸葛青");
        values2.put("age",25);
        resolver.insert(uri1,values2);
        ContentValues values3=new ContentValues();
        values3.put("name","查理苏");
        values3.put("age",28);
        resolver.insert(uri1,values3);
        ContentValues values4=new ContentValues();
        values4.put("name","左然");
        values4.put("age",29);
        resolver.insert(uri1,values4);
        ContentValues values5=new ContentValues();
        values5.put("name","李泽言");
        values5.put("age",28);
        resolver.insert(uri1,values5);
        ContentValues values6=new ContentValues();
        values6.put("name","喻文州");
        values6.put("age",23);
        resolver.insert(uri1,values6);

        list=MyqueryAll(resolver.query(uri, context.databaseList(),
                "select * from person",new String[]{},null));


        imagedata = new ArrayList<>();
        //图片数据
        int[] image={R.drawable.img,R.drawable.img_6,R.drawable.img_2
                ,R.drawable.img_3,R.drawable.img_4,R.drawable.img_5};
        for (int i=0;i<image.length;i++)
        {
            HashMap<String,Object> map=new HashMap<>();
            map.put("头像",image[i]);
            imagedata.add(map);
        }

        adapter=new Myadapter(context,list,imagedata);//list给adpter
        recyclerview.setAdapter(adapter);


        //launcher 启动项
        ActivityResultLauncher launcher=
                registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                        new ActivityResultCallback<ActivityResult>() {
                            @Override
                            public void onActivityResult(ActivityResult result) {
                                if (result.getResultCode() == 520) {
                                    Intent data = result.getData();
                                    String name = data.getStringExtra("name"); // 获取返回的姓名数据
                                    int age = data.getIntExtra("age", 0);       // 获取返回的年龄数据

                                    // 将数据插入到数据库
                                    ContentValues values = new ContentValues();
                                    values.put("name", name);
                                    values.put("age", age);
                                    resolver.insert(uri, values);

                                    Cursor cursor = resolver.query(uri, context.databaseList(),
                                            "select * from person",new String[]{},null);
                                    int recordCount = cursor.getCount();
                                    cursor.close();
                                    // 更新RecyclerView
                                    HashMap<String, Object> newItem = new HashMap<>();
                                    newItem.put("name", name);
                                    newItem.put("id", recordCount);
                                    newItem.put("age", age);

                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("头像", R.drawable.img_7);

                                    adapter.addItem(newItem, map, values);
                                }
                            }
                        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InputActivity.class);
                launcher.launch(intent);
            }
        });

        LinearLayoutManager manager=new LinearLayoutManager(context);
        manager.setOrientation(RecyclerView.VERTICAL);
        recyclerview.setLayoutManager(manager);//控制列表上下左右
        // Inflate the layout for this fragment
        return view;//压缩，整个屏幕的xml压缩到局部
    }

    public ArrayList<HashMap<String,Object>> MyqueryAll(Cursor cursor1) {
        Cursor cursor=cursor1;
        ArrayList<HashMap<String,Object>> list = new ArrayList<>();
        cursor.moveToFirst();//cursor每次走到头遍历
        while(!cursor.isAfterLast()){
            @SuppressLint("Range") int id=cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String name=cursor.getString(cursor.getColumnIndex("name"));
            @SuppressLint("Range") int age=cursor.getInt(cursor.getColumnIndex("age"));
            //stringBuilder.append(id+name+age);
            HashMap<String,Object> map=new HashMap<>();
            map.put("id",id);
            map.put("name",name);
            map.put("age",age);
            list.add(map);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
}
