package com.example.mylovechat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;


public class Fragment3 extends Fragment {

    Button queryButton;
    EditText queryEditText;
    ArrayList<HashMap<String,Object>> result;
    Context context;
    ImageView imageView;
    TextView textView_id,textView_age;

    private ContentResolver resolver;
    private static final String[] PROJECTION = new String[]{"id", "name", "age"};
    private static final String AUTHORITY ="lwy.light.provider";
    private static final Uri NOTIFY_URI=Uri.parse("content://"+AUTHORITY+"/person");
    Uri uri=Uri.parse("content://lwy.light.provider/person");

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.tab3, container, false);//压缩，整个屏幕的xml压缩到局部

        // 获取需要操作的控件引用
        queryButton = view.findViewById(R.id.button_select);
        textView_id=view.findViewById(R.id.textView_select_id);
        textView_age=view.findViewById(R.id.textView_select_age);

        // 设置按钮点击事件监听器
        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 在按钮点击事件中执行查询操作
                queryEditText = getView().findViewById(R.id.EditText_select_name);
                String queryText = queryEditText.getText().toString();
                Log.d("lwy","读取："+queryText);
                result=LoveQuery(queryText);
                //Log.d("lwy","LoveQuery");
                if(result.isEmpty()){
                    textView_id.setText("他被你删了！");
                    textView_age.setText("打入冷宫了！");
                } else {
                    textView_id.setText("id:"+result.get(0).get("id").toString());
                    textView_age.setText("age:"+result.get(0).get("age").toString());
                }
            }
        });

        return view;
    }

    private ArrayList<HashMap<String,Object>> LoveQuery(String queryText) {
        context=getContext();
        resolver=context.getContentResolver();
        Log.d("lwy","resolver");
        String[] selectionArgs = new String[]{queryText};
        Cursor cursor = resolver.query(uri, PROJECTION,
                "select * from person where name=?" , selectionArgs, null);
        Log.d("lwy","个数："+cursor.getCount());
        ArrayList<HashMap<String,Object>> list = new ArrayList<>();;//value不写死，写object
        cursor.moveToFirst();//cursor每次走到头遍历
        while(!cursor.isAfterLast()){
            @SuppressLint("Range") int id=cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String name=cursor.getString(cursor.getColumnIndex("name"));
            @SuppressLint("Range") int age=cursor.getInt(cursor.getColumnIndex("age"));
            //stringBuilder.append(id+name+age);
            HashMap<String,Object> map=new HashMap<>();
            map.put("id",id);
            map.put("name",name);
            map.put("age",age);
            list.add(map);
            cursor.moveToNext();
        }

        cursor.close();
        return list;
    }
}