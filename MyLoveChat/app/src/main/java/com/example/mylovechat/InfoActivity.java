package com.example.mylovechat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InfoActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView1,textView2,textView3,textView4;
    ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        textView1=findViewById(R.id.person_name);
        textView2=findViewById(R.id.person_age);
        textView3=findViewById(R.id.person_city);
        textView4=findViewById(R.id.textView_info);
        imageView=findViewById(R.id.person_love);
        imageButton=findViewById(R.id.person_back);

        Intent intent=getIntent();
        String name=intent.getExtras().getString("name");
        String age=intent.getExtras().getString("age");
        String id=intent.getExtras().getString("id");
        int img=intent.getExtras().getInt("image");

        textView1.setText("姓名："+name);
        textView2.setText("年龄："+age);
        textView3.setText("编号："+id);
        imageView.setImageResource(img);

        String filename="people"+id;//不加后缀名
        String fileContent = readFileContent(filename);

        textView4.setText(fileContent);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private String readFileContent(String filename) {
        StringBuilder content = new StringBuilder();
        try {
            // 通过文件名获取资源ID
            //getResources().getIdentifier()方法是一个用于获取资源ID的方法。它接受三个参数：资源名称、资源类型和包名。
            int resourceId = getResources().getIdentifier(filename, "raw", getPackageName());
            if (resourceId != 0) {
                InputStream inputStream = getResources().openRawResource(resourceId);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    content.append(line);
                    content.append("\n");
                }
                reader.close();
            } else {
                // 资源ID为0，表示文件名对应的资源不存在
                // 处理资源不存在的情况
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}