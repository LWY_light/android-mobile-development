package com.example.mylovechat;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class InputActivity extends AppCompatActivity {

    Button button,button2;
    EditText editText1,editText2;
    ImageView imageView;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        imageView=findViewById(R.id.imageView_input_add);
        button=findViewById(R.id.button_input_add);
        button2=findViewById(R.id.button_input_return);
        editText1=findViewById(R.id.EditText_input_name);
        editText2=findViewById(R.id.EditText_input_age);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(555); // 直接返回设置结果代码为555
                finish();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editText1.getText().toString();
                String ageText = editText2.getText().toString();

                // 检查姓名和年龄是否为空
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(ageText)) {
                    // 输入数据不完整，显示错误提示或执行其他操作
                    // 例如，显示一个Toast提示用户输入数据不完整
                    Toast.makeText(InputActivity.this, "请输入姓名和年龄", Toast.LENGTH_SHORT).show();
                } else {
                    int age = Integer.parseInt(ageText);

                    // 在输入数据完成后的保存按钮点击事件中
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("name", name); // 将输入的姓名放入Intent中
                    resultIntent.putExtra("age", age);   // 将输入的年龄放入Intent中
                    setResult(520, resultIntent);
                    finish(); // 结束当前Activity，返回到调用它的Activity
                }
            }
        });
    }
}