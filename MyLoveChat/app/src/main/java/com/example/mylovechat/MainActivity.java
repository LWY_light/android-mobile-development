package com.example.mylovechat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Fragment fragment1,fragment2,fragment3;


    FragmentManager fm;//fragment管理器
    //FragmentTransaction ft;//fragment交互（通信一对一，交互是批量动作，带commit），放外面不利于事务独立性，函数都可以用

    LinearLayout linearLayout1,linearLayout2,linearLayout3;


    @SuppressLint("MissingInflatedId")

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        linearLayout1=findViewById(R.id.LinearLayout1);
        linearLayout2=findViewById(R.id.LinearLayout2);
        linearLayout3=findViewById(R.id.LinearLayout3);

        fragment1=new Fragment1();
        fragment2=new Fragment2();
        fragment3=new Fragment3();
        fm=getSupportFragmentManager();
        initial();//初始化

        fragmenthide();//隐藏字幕
        fragmentshow(fragment2);//默认聊天界面

        //全局监听
        linearLayout1.setOnClickListener(this);
        linearLayout2.setOnClickListener(this);
        linearLayout3.setOnClickListener(this);
    }

    private void fragmenthide() {
        FragmentTransaction ft=fm.beginTransaction()
                .hide(fragment1)
                .hide(fragment2)
                .hide(fragment3);//隐藏之后涉及点击功能
        ft.commit();
    }//初始隐藏字幕

    private void initial() {

        FragmentTransaction ft=fm.beginTransaction()
                .add(R.id.content1,fragment1)
                .add(R.id.content1,fragment2)
                .add(R.id.content1,fragment3);
        ft.commit();
    }//初始化


    @Override
    public void onClick(View view) {

        fragmenthide();

        int id=view.getId();
        if(id==R.id.LinearLayout1) {
            fragmentshow(fragment1);
        }
        if(id==R.id.LinearLayout2) {
            fragmentshow(fragment2);
        }
        if(id==R.id.LinearLayout3){
            fragmentshow(fragment3);
        }

    }//点击

    private void fragmentshow(Fragment fragment) {
        FragmentTransaction ft=fm.beginTransaction()
                .show(fragment);
        ft.commit();
    }//显示
}