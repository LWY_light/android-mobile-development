package com.example.mylovechat;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class MyContentProvider extends ContentProvider {

    MyDAO1 myDAO;
    public MyContentProvider() {
    }
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return myDAO.Mydelete(selection,selectionArgs);
    }
    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        return myDAO.Myinsert(values);
    }
    @Override
    public boolean onCreate() {
        Context context=getContext();
        myDAO=new MyDAO1(context);
        return true;
    }
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
      return myDAO.Myquery(selection,selectionArgs);
    }
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        return myDAO.Myupdate(values,selection,selectionArgs);
    }
}