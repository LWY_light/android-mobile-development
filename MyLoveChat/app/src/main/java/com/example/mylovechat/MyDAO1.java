package com.example.mylovechat;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

public class MyDAO1 {

    SQLiteOpenHelper myopenhelper;
    SQLiteDatabase database;

    Context mycontext;

    Uri uri=Uri.parse("content://lwy.light.provider");

    public MyDAO1(Context context) {
        mycontext=context;
        myopenhelper=new MyOpenHelper(context,"lwyDB",null,1);
        database=myopenhelper.getWritableDatabase();
        database.execSQL("drop table if exists person");//如果表已经存在就删除重新生成
        database.execSQL("CREATE TABLE person (\n" +
                "    id Integer PRIMARY KEY autoincrement,\n" +
                "    name VARCHAR(50),\n" +
                "    age INT\n" +
                ");");//建表
    }
    public Cursor Myquery (String selection, String[] selectionArgs) {
        Cursor cursor=database.rawQuery(selection,selectionArgs);
        return cursor;
    }

    public Uri Myinsert(ContentValues values) {

        Long row_id=database.insert("person",null,values);

        if (row_id==-1){
            Log.d("DAO","数据插入失败");
            return null;
        } else {
            Uri insertUri= ContentUris.withAppendedId(uri,row_id); //在原先的uri加上行号
            mycontext.getContentResolver().notifyChange(insertUri,null);
            return insertUri;
        }
    }

    public int Myupdate(ContentValues values, String selection, String[] selectionArgs) {
        int rowsAffected = database.update("person",values,selection,selectionArgs);
        mycontext.getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    public int Mydelete(String selection, String[] selectionArgs) {
        int rowsDeleted = database.delete("person",selection,selectionArgs);;
        mycontext.getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }
}
