package com.example.mylovechat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

public class Myadapter extends RecyclerView.Adapter<Myadapter.Myholder> {

    Context context1;
    ArrayList<HashMap<String,Object>> list1;//没有给定节点类型
    ArrayList<HashMap<String,Object>> imagedata; //图片数据

    private final ContentResolver resolver;
    private static final String[] PROJECTION = new String[]{"id", "name", "age"};
    private static final String AUTHORITY ="lwy.light.provider";
    private static final Uri NOTIFY_URI=Uri.parse("content://"+AUTHORITY+"/person");
    Uri uri=Uri.parse("content://lwy.light.provider/person");


    public Myadapter(Context context,ArrayList<HashMap<String,Object>> list,ArrayList<HashMap<String,Object>> list2) {
        context1=context;
        list1=list;
        imagedata=list2;
        resolver=context1.getContentResolver();
    }

    public void addItem(HashMap<String, Object> item, HashMap<String, Object> item2, ContentValues values) {
        list1.add(item);
        notifyItemInserted(list1.size() - 1);
        imagedata.add(item2);
        notifyItemInserted(imagedata.size() - 1);
    }


    //泛型传入=类嵌套 类名<传入类名>
    @NonNull
    @Override

    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Inflater,java的真的zip压缩器,不是我们要的

        View view=LayoutInflater.from(context1).inflate(R.layout.item,parent,false); //官方压缩器
        //inflater返回值是一个view（压缩后的）
        Myholder myholder=new Myholder(view);
        return myholder;//把每行数据inflater压缩
    }

    @Override
    public void onBindViewHolder(@NonNull Myholder holder, @SuppressLint("RecyclerView") int position) {

        HashMap<String, Object> map = list1.get(position);
        String name = (String) map.get("name");
        holder.textView.setText(name);
        String id=map.get("id").toString();
        holder.textView2.setText("编号:"+id);
        String age=map.get("age").toString();
        holder.textView3.setText("年龄："+age);
        holder.imageView.setImageResource((int)(imagedata.get(position).get("头像")));
        holder.personButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context1, InfoActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("age",age);
                intent.putExtra("id",id);
                intent.putExtra("image",(int)(imagedata.get(position).get("头像")));

                context1.startActivity(intent);
            }
        });


    }//bind绑定,连接list数据和viewholder(指向行的指针，java没有指针，自定义viewholder的子类因为每行数据类型不确定)

    @Override
    public int getItemCount() {
        return list1.size();
    }//数据绑定，list有多少行

    public class Myholder extends RecyclerView.ViewHolder{

            TextView textView,textView2,textView3;
            ImageView imageView;

            Button personButton;
        public Myholder(@NonNull View itemView) {
            super(itemView);

            textView=itemView.findViewById(R.id.textView_wechat);
            textView2=itemView.findViewById(R.id.textView_id);
            textView3=itemView.findViewById(R.id.textView_age);
            imageView=itemView.findViewById(R.id.imageView_wechat);
            personButton=itemView.findViewById(R.id.button_person);
        }

    }//泛型传入viewholder类
}
