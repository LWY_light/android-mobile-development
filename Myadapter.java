package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.zip.Inflater;

public class Myadapter extends RecyclerView.Adapter<Myadapter.Myholder> {

    Context context1;
    List<String> list1;//没有给定节点类型
    public Myadapter(Context context,List list) {
        context1=context;
        list1=list;
    }

    //泛型传入=类嵌套 类名<传入类名>
    @NonNull
    @Override

    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Inflater,java的真的zip压缩器,不是我们要的

        View view=LayoutInflater.from(context1).inflate(R.layout.item,parent,false); //官方压缩器
        //inflater返回值是一个view（压缩后的）
        Myholder myholder=new Myholder(view);
        return myholder;//把每行数据inflater压缩
    }

    @Override
    public void onBindViewHolder(@NonNull Myholder holder, @SuppressLint("RecyclerView") int position) {

        holder.textView.setText(list1.get(position));//get也不确定返回类型,list要给定，自动返回string

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //intent:从main跳到chat，mainactivity挂在后台会stop停止
                Intent intent=new Intent(context1,ChatActivity.class); //消息封装在intent里面，intent可以理解为一种通讯方法
                intent.putExtra("name",list1.get(position));
                context1.startActivity(intent);
            }
        });

    }//bind绑定,连接list数据和viewholder(指向行的指针，java没有指针，自定义viewholder的子类因为每行数据类型不确定)

    @Override
    public int getItemCount() {
        return list1.size();
    }//数据绑定，list有多少行

    public class Myholder extends RecyclerView.ViewHolder{

            TextView textView;
            Button button;
        public Myholder(@NonNull View itemView) {
            super(itemView);

            textView=itemView.findViewById(R.id.textView1);
            button=itemView.findViewById(R.id.button_chat);
        }

    }//泛型传入viewholder类
}
